<?php

/**
 * UserController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of UserController
 *
 * @author 
 */
class UserController extends AController
{
	public $layout = '/layout/browse';
	/**
	 * index of users, show all users and activity etc
	 */
	public function actionIndex()
	{
		$this->render('index');
	}
	
	/**
	 * user profile show a users profile,
	 * posts they have written, 
	 * questions they have asked / answered etc
	 * @param int $id
	 */
	public function actionProfile($id)
	{
		$user = NData::loadModel('User', $id, 'No user found');
		$this->render('profile', array('user'=>$user));
	}
	
	public function actionViewed()
	{
		$this->render('viewed');
	}
}