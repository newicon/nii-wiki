<?php

/**
 * SearchController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of SearchController
 *
 * @author 
 */
class SearchController extends AController
{
	public $layout = '/layout/browse';
	
	public function init()
	{
		require_once('Zend/Search/Lucene.php');
		require_once('StandardAnalyzer/Analyzer/Standard/English.php');
		parent::init();
	}
	
	/**
	 * The action that handles search queries
	 * @param string $q expect GET variable q
	 */
	public function actionIndex()
	{
		if (($q = Yii::app()->getRequest()->getParam('q', null)) !== null) {
			$results = WikiSearch::get()->find($q);
			// we have done a search lets store it.
			WikiQuery::add($q);
		}
		$this->render('index', compact('q', 'results'));
	}
	
	/**
	 * show a list of searches,
	 * organise by most popular search
	 * most freaquent search yeilding few results
	 * this page enables people to see what information is being searched for
	 * and whether the search is finding it. This can help make descisions on 
	 * what to articles or questions to post next
	 */
	public function actionRecord()
	{
		$wq = WikiQuery::model()->findAll();
		$this->render('record', compact('wq'));
		WikiQuery::searchFrequency();
	}
	
	/**
	 * action to rebuild the search lucene index
	 */
	public function actionBuildIndex()
	{
		WikiSearch::get()->buildIndex();
		Yii::app()->user->setFlash('success', 'Wiki index rebuilt success.  Wiki search index robot = happy. Search should now work, and be awesome!');
		$this->redirect(array('/wiki/index/index'));
	}
}