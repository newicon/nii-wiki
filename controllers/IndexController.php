<?php

/**
 * Email class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Wiki Index
 */
class IndexController extends AController
{
	
	public $layout = '/layout/browse';
	
	/**
	 * Index home page for the wiki
	 */
	public function actionIndex()
	{
		WikiStat::bestUser();
		$this->title = 'Wiki';
		$this->render('index');
	}
	
	/**
	 * Display articles page
	 */
	public function actionArticles()
	{
		$this->render('articles');
	}
	
	/**
	 * Display a wiki page,
	 * A page can be a document (PDF, Word, Power point, Excel) a wiki article, or a link
	 * @param int $id
	 */
	public function actionPage($slug)
	{
		$model = WikiPage::model()->findByAttributes(array('slug'=>$slug));
		if ($model===null) {
			// the page does not exist so lets offer to create a new one
			$this->redirect(array('/wiki/index/edit', 'title'=>$slug));
		}
		
		WikiView::logViewed($model, $model->author_id);
		$this->render('page', array('model'=>$model));
	}
	
	/**
	 * Edit a wiki page
	 * @param int $id pk [optional] if null will create a new model
	 * @param string $title [optional] title to give this wiki
	 */
	public function actionEdit($id=null, $title='')
	{
		Yii::app()->getModule('wiki')->displayHeader = false;
		if (empty($id)) {
			$model = new WikiPage;
			$model->title = $title;
		} else
			$model = NData::loadModel('WikiPage', $id, "No wiki page found with id '$id'");
		
		$this->performAjaxValidation($model, 'wiki-page');

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_POST['WikiPage'];
			$model->save();
			if (isset($_POST['SaveExit'])) {
				$this->redirect($model->route);
			}
		}
		
		$this->render('edit', array(
			'model'=>$model
		));
	}
	
	/**
	 * Soft delete a wiki article
	 */
	public function actionDelete($id)
	{
		$wp = NData::loadModel('WikiPage', $id, 'No wikwi page found');
		$wp->remove();
		$wp->save();
		Yii::app()->user->setFlash('success', 'Successfully removed wiki article. <a class="btn" href="'.NHtml::url(array('/wiki/index/restore','id'=>$id)).'"><i class="icon-arrow-right"></i> Undo</a>');
		$this->redirect(array('/wiki/index/index'));
	}
	
	/**
	 * restore an article that has been removed "trashed"
	 * @see self::actionDelete
	 */
	public function actionRestore($id)
	{
		$wp = WikiPage::model()->withRemoved()->findByPk($id);
		if ($wp === null)
			throw new CHttpException('No wiki page found');
		$wp->restore();
		$wp->save();
		Yii::app()->user->setFlash('success', 'Successfully restored wiki article. ');
		$this->redirect($wp->route);
	}
	
	public function actionExport()
	{
		$wp = WikiPage::model()->findAll();
		$data = array();
		foreach ($wp as $p) {
			$data[] = $p->attributes;
		}
		dp(json_encode($data, JSON_PRETTY_PRINT));
	}
}