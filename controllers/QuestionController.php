<?php

/**
 * QuestionController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of QuestionController
 *
 * @author 
 */
class QuestionController extends AController
{
	public $layout = '/layout/browse';
	
	/**
	 * list questions
	 */
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionUnanswered()
	{
		$this->render('unanswered');
	}
	
	/**
	 * View and save wiki questions
	 */
	public function actionAsk($id=null, $title='')
	{
		if (empty($id)) {
			$model = new WikiQuestion();
			$model->title = $title;
		} else {
			$model = NData::loadModel('WikiQuestion', $id, "No Question found with id '$id'");
		}
		
		$this->performAjaxValidation($model, 'wiki-question');
		
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_POST['WikiQuestion'];
			$model->tags = $_POST['WikiQuestion']['tags'];
			$model->save();
			if ($model->validate())
				$this->redirect(array('/wiki/question/view', 'id'=>$model->id));
		}
		$this->render('ask', array('model'=>$model));
	}
	
	/**
	 * Soft delete a question
	 */
	public function actionDelete($id)
	{
		$q = NData::loadModel('WikiQuestion', $id, "Unable to find a question with id '$id'");
		$q->trash();
		Yii::app()->user->setFlash('success', 'Successfully removed question. <a class="btn" href="'.NHtml::url(array('/wiki/question/restore','id'=>$id)).'"><i class="icon-arrow-right"></i> Undo</a>');
		$this->redirect(array('/wiki/question/index'));
	}
	
	/**
	 * Restore a wiki question / answer
	 */
	public function actionRestore($id)
	{
		$wq = WikiQuestion::model()->withRemoved()->findByPk($id);
		if ($wq === null)
			throw new CHttpException('No question found');
		$wq->restore();
		$wq->save();
		Yii::app()->user->setFlash('success', 'Successfully restored wiki question.');
		$this->redirect($wq->route);
	}
	
	/**
	 * Delete an answer called via ajax for use in the question detail view
	 * @param $is the id of the answer to delete
	 */
	public function actionAnswerDelete($id)
	{
		$a = NData::loadModel('WikiAnswer', $id, "Unable to find an answer with id '$id'");
		$a->trash();
	}
	
	/**
	 * process the answer question form,
	 * save an answer if valid,
	 * return the new answer in json form
	 * @param int $id the question id this answer answers
	 */
	public function actionAnswer($id)
	{
		$answer = new WikiAnswer;
		//$this->performAjaxValidation($answer, 'wiki-answer');
		
		$q = NData::loadModel('WikiQuestion', $id, "No Question found with id '$id'");
		
		$success = false;
		if (isset($_POST['WikiAnswer'])) {
			$answer->attributes = $_POST['WikiAnswer'];
			$answer->question_id = $q->id;
			$success = $answer->save();
		}
		
		if ($success)
			echo $answer->toJson();
		else
			echo '{}';
	}
	
	/**
	 * accept an answer
	 * id is the answer id
	 * two POST params:
	 * @param int $question question pk
	 * @param int $answer answer pk the accepted answer for the $question
	 */
	public function actionAnswerAccept()
	{
		// Check the answer with the posted id exists:
		$a = NData::loadModel('WikiAnswer', $_POST['answer'], "Unable to find an answer with id '{$_POST['answer']}'");
		
		// Need to set this answer to be the officially accepted answer by updating the 
		// accepted_answer_id field in the root question record
		
		// Get the question 
		$q = NData::loadModel('WikiQuestion', $_POST['question'], "Unable to find a question with id '{$_POST['question']}'");
		
		// Only the owner of the question can set an answer as accepted
		if ($q->owner_id == Yii::app()->user->id) {
			$q->setAcceptedAnswer($a->id);
		}
	}
	
	/**
	 * Show an individual question
	 */
	public function actionView($id)
	{
		$q = NData::loadModel('WikiQuestion', $id, 'No question with this id exists.');
		WikiView::logViewed($q, $q->owner_id);
		$this->render('view', array(
			'q'=>$q,
		));
	}
	
}