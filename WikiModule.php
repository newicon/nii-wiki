<?php

/**
 * SupportModule class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of SupportModule
 *
 * @author 
 */
class WikiModule extends NWebModule
{
	public $name = 'Wiki Module';
	public $description = 'Wiki Module';
	public $version = '0.1.0';
	/**
	 * A way for controller actions to hide and show the header search bar, defaults to show
	 * @var boolean 
	 */
	public $displayHeader = true;
	
	/**
	 * initialise the project module
	 * @return void
	 */
	public function init()
	{
		Yii::import('wiki.vendor.*');
		Yii::import('wiki.models.*');
		Yii::import('wiki.widgets.*');
		Yii::import('wiki.components.*');
		Yii::import('wiki.extensions.*');
		Yii::app()->menus->addItem('main', 'Wiki', array('/wiki/index/index'), null, array('order'=>60, 'icon'=>'icon-list-alt'));
		Yii::app()->urlManager->addRules(array(
			'/wiki-article/<slug:.*?>'=>'/wiki/index/page'
		), true);
	}
	
	public function setup() 
	{
		// attach a wiki behavior to the user
		Yii::app()->user->attachBehavior('wiki', array('class'=>'wiki.components.WikiUserBehavior'));
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl().'/css/wiki.css');
	}
	
	public static function activityWikiPublished($log)
	{
		$wp = WikiPage::model()->findByPk($log->model_id);
		if ($wp === null) {
			return array(
				'title'=>'Published a wiki article',
				'body'=>'',
			);
		}
		$img = Yii::app()->getModule('wiki')->getAssetsUrl().'/img/blue-document-text.png';
		return array(
			'title'=>'',
			'body'=>'
				<div class="media mtn">
					<a href="'.$wp->link.'" class="pull-left">
						<img src="'.$img.'"/> <a href="'.$wp->link.'">
					</a>
					<div class="media-body">
						Published wiki article <br/>
						<a href="'.$wp->link.'">'.$wp->title.'</a>
					</div>
				</div>
			',
			'link'=>$wp->link
		);
	}
	
	public function install()
	{
		NActiveRecord::install('WikiPage');
		NActiveRecord::install('WikiQuestion');
		NActiveRecord::install('WikiFollow');
		NActiveRecord::install('WikiQuery');
		NActiveRecord::install('WikiView');
	}
}