<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<div class="doc" style="width:225px;">
<div class="media">
	<a class="pull-left" href="#">
		<?php echo $user->profileImage; ?>
	</a>
	<div class="media-body">
		<strong><a href="<?php echo NHtml::url(array('/wiki/user/profile','id'=>$user->id)); ?>"><?php echo $user->name; ?></a></strong>
		<br/>Served: <span class="badge"><?php echo WikiStat::statTotalServed($user->id) ?></span>
	</div>
</div>
<?php echo WikiStat::displayUserStat($user->id); ?>
<!-- different user so lets show the follow tools -->
<?php if ($user->id != Yii::app()->user->id): ?>
	
	<?php if (WikiFollow::isFollowing($user->id)): ?>
		<a class="btn btn-primary" href="">Unfollow</a>
	<?php else: ?>
		<a class="btn btn-primary" href="">Follow</a>
	<?php endif; ?>
<?php endif; ?>
</div>