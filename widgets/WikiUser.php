<?php

/**
 * WikiUser class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * This class is used to gather stats and display a nice user profile summary detailing activity of this user on the wiki
 *
 */
class WikiUser extends NWidget
{
	/**
	 * User model optional,
	 * if null will use Yii::app()->user->record
	 */
	public $user = null;
	
	public function run()
	{
		if ($this->user === null)
			$this->user = Yii::app()->user->record;
		
		$this->render('wiki-user', array(
			'user'=>$this->user
		));
	}
}