<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<div class="well" style="max-width: 200px; padding: 8px 0;">
	<ul class="nav nav-list">
		<li class="nav-header">Browse</li>
		<li class="<?php echo NHtml::active('/wiki/index/index') ?>"><a href="<?php echo NHtml::url('/wiki/index/index') ?>">Home</a></li>
		<li class="<?php echo NHtml::active('/wiki/user/profile/id/'.Yii::app()->user->id) ?>"><a href="<?php echo NHtml::url(array('/wiki/user/profile','id'=>Yii::app()->user->id)) ?>">Me</a></li>
		<li class="<?php echo NHtml::active('/wiki/index/articles') ?> <?php echo NHtml::active('/wiki/index/page') ?>"><a href="<?php echo NHtml::url('/wiki/index/articles') ?>">Articles</a></li>
<!--		<li class="">Documents</li>
		<li class="">Links</li>-->
		<li class="<?php echo NHtml::active('/wiki/question') ?>">
			<a href="<?php echo NHtml::url('/wiki/question/index'); ?>">Questions <?php echo WikiHtml::unansweredBadge(); ?></a>
		</li>
		<li class="nav-header">Categories</li>
		<li class="nav-header">Tags</li>
	</ul>
</div>
<div class="well" style="max-width: 200px; padding: 8px 4px;">
	<?php foreach(User::model()->findAll(array('order'=>'first_name')) as $user): ?>
	<div class="media">
		<div class="pull-left">
			<?php echo WikiHtml::userImage($user, 24) ?>
		</div>
		<div class="media-body">
			<a href="<?php echo NHtml::url(array('/wiki/user/profile', 'id'=>$user->id)) ?>"><?php echo $user->name; ?></a>
		</div>
	</div>
	<?php endforeach; ?>
</div>