<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<style>
/* Large desktop */
@media (min-width: 1200px) {}

/* Large desktop */
@media (min-width: 980px) and (max-width: 1200px) { 
	/*.navbar .nav li a {padding:25px 15px 15px;}*/
	.navbar .navbar-inner{padding-right:0px;}
}
 
/* Portrait tablet to landscape and desktop */
@media (min-width: 768px) and (max-width: 979px) { 
	.navbar .navbar-inner{padding:0px 0px 0px 0px;}
}
 
/* Landscape phone to portrait tablet */
@media (max-width: 767px) { 
	/* for some reason bootstrap think it sensible to add padding to the body at this point. */
	body {padding:0px;}
	.navbar .navbar-inner{padding:0px 0px 0px 0px;}
	.container {padding:0px 20px}
}
 
/* Landscape phones and down */
@media (max-width: 480px) { 
	/* for some reason bootstrap think it sensible to add padding to the body at this point. */
	body {padding:0px;}
	.navbar .navbar-inner{padding:0px 0px 0px 0px;}
	.container {padding:0px 20px}
}

.doc{background-color:#fff;padding:10px 10px;; border:1px solid #ccc;border-radius:4px;box-shadow:0px 2px 3px rgba(0,0,0,0.3)}
.doc-inner{padding:30px 80px 70px;}
html{min-height:100%;}
body{background: #ffffff; /* Old browsers */background: -moz-linear-gradient(top,  #fff 0%, #f1f1f1 500px); /* FF3.6+ */background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fff), color-stop(500px,#f1f1f1)); /* Chrome,Safari4+ */background: -webkit-linear-gradient(top,  #ffffff 0%,#f1f1f1 500px); /* Chrome10+,Safari5.1+ */background: -o-linear-gradient(top,  #ffffff 0%,#f1f1f1 500px); /* Opera 11.10+ */background: -ms-linear-gradient(top,  #ffffff 0%,#f1f1f1 500px); /* IE10+ */background: linear-gradient(to bottom,  #ffffff 0%,#f1f1f1 500px); /* W3C */filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f1f1f1',GradientType=0 ); /* IE6-9 */}

</style>

<div class="line">
<!--	<div class="unit" style="width:220px">
		&nbsp;
	</div>-->
	<!--<div class="lastUnit ">-->
	<?php if (!file_exists(WikiSearch::get()->getIndexDirectory())):  ?>
	<div class="alert alert-error">The wiki index needs to be rebuilt for the search to work (and you want it to work because it's awesome, if I say so myself). <a href="<?php echo NHtml::url('wiki/search/buildIndex'); ?>" class="btn btn-error">Re-build Index (Yep, don't think, just click me)</a></div>
	<?php endif; ?>
	<?php if (Yii::app()->getModule('wiki')->displayHeader): ?>
		<div class="pull-right">
			<div class="unit">
				<form id="wiki-search" action="<?php echo NHtml::url('/wiki/search/index'); ?>" method="get">
				<div class="search-bar" style="width:582px">
					<div class="input-append input-block-level">
						<input id="wiki-q" name="q" type="text" value="<?php echo isset($_GET['q']) ? $_GET['q'] : '';  ?>" style="width:500px" width placeholder="What do you want to know?" />
						<button type="submit" onclick="if($('#wiki-q').val()=='')return false;" class="btn">Search</button>
					</div>
				</div>
				</form>
			</div>
			<div class="unit plm">
				<div class="btn-group">
					<a href="<?php echo NHtml::url('wiki/question/ask'); ?>" onclick="var q=$('#wiki-q').val();this.href=(q=='')?this.href:this.href+'?title='+q;" class="btn btn-primary" style="padding:13px 25px;">Ask</button>
					<a href="<?php echo NHtml::url('wiki/index/edit'); ?>" onclick="var q=$('#wiki-q').val();this.href=(q=='')?this.href:this.href+'?title='+q;" class="btn btn-primary" style="padding:13px 25px;">Post</a>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<!--</div>-->
</div>