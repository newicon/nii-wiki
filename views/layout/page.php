<?php

/**
 * full page
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<?php $this->beginContent('//layouts/admin1column'); ?>

<?php $this->renderPartial('/layout/_header'); ?>

<?php echo $content; ?>

<?php $this->endContent(); ?>
