<?php
/**
 * Nii view file
 * Layout file with sidebar
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<?php $this->beginContent('//layouts/admin1column'); ?>

<?php $this->renderPartial('/layout/_header'); ?>

<div class="row-fluid">
	
	<div class="span2 wiki-sidebar">
		<?php $this->renderPartial('/layout/_sidebar') ?>
	</div>
	
	<div class="span10">
		<?php echo $content ?>
	</div>
</div>

<?php $this->endContent(); ?>