<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<div class="line">
	<div class="unit">
		<div style="width:225px;">
			<a onclick="$('#modal-user-account').modal('show');return false;" href="gravatar.com/emails" rel="tooltip" title="Change your avatar at gravatar.com"><img class="thumbnail" src="<?php echo $user->getProfileImageUrl(210); ?>" /></a>
			<h1 class="profile-name" style="font-size:24px;"><?php echo $user->name; ?></h1>
			<br/><span class="badge badge-info"><?php echo WikiStat::statTotalServed($user->id); ?></span> Served<br/>
			<?php echo WikiStat::displayUserStat($user->id); ?>
			<!-- different user so lets show the follow tools -->
			<?php if ($user->id != Yii::app()->user->id): ?>
				<?php if (WikiFollow::isFollowing($user->id)): ?>
					<a class="btn btn-primary" href="">Unfollow</a>
				<?php else: ?>
					<a class="btn btn-primary" href="">Follow</a>
				<?php endif; ?>
			<?php endif; ?>
			
			<!-- Position -->
			<!-- todo: gamify a little
				Your 1st! meaning you have the most articles which are the most popular (served the most people)
				Perhaps an amount of content you have produced as an average to everyone else:
				You have produced 20% more content then the average member
				You have produced 10% less content than the average member?
				You have consumed 30% more than the average user
   
				Could also display the list of users you have recently serverd when clicking on the serverd stat.
				WikiView::getUsersServedBy($user->id)
			-->
			
			<?php if(key(WikiStat::bestUser()) == Yii::app()->user->id):  ?>
				<br />
				<div class="alert alert-info">
					<p><i class="icon-gift"></i> <strong>You are the best!</strong> <br/>Your articles have helped the most people at Newicon!</p>
				</div>
			<?php endif; ?>
			
		</div>
	</div>
	<div class="lastUnit" style="padding-left:20px;">
		
		<?php if ($user->id == Yii::app()->user->id) : ?>
		
			<?php $drafts = WikiData::getMyDraftPosts($user->id); ?>
			<?php if (count($drafts)) : ?>
			<h3>My Draft</h3>
			<?php foreach($drafts as $page): ?>
				<a href="<?php echo NHtml::url($page->route); ?>"><?php echo $page->title; ?></a> <br/>
			<?php endforeach; ?>
			<?php endif; ?>

			<!-- Trashed -->
			<?php $trashed = WikiData::getMyTrashedPosts($user->id); ?>
			<?php if(count($trashed)): ?>
			<h3>Trashed</h3>
			<?php foreach($trashed as $page): ?>
				<a href="<?php echo NHtml::url($page->route); ?>"><?php echo $page->title; ?></a> <br/>
			<?php endforeach; ?>
			<?php endif; ?>
				
		<?php endif; ?>

		<hr>
		
		<h3>Posts</h3>
		<?php $posts = WikiData::getUserPostsPublished($user->id); ?>
		<?php if (count($posts)) : ?>
			<?php foreach(WikiPage::model()->author($user->id)->published()->findAll() as $page): ?>
				<a href="<?php echo NHtml::url($page->route); ?>"><?php echo $page->title; ?></a> <br/>
			<?php endforeach; ?>
		<?php else: ?>
			No Posts Yet
		<?php endif; ?>
			
		<hr />
		
		<!-- Your Questions -->
		<?php 
			$s = new WikiQuestion('search');
			$dataProvider=new CActiveDataProvider($s->with('user')->owner($user->id)->newest()->questions());
		?>
		<h3>Questions <span class="badge"><?php echo $dataProvider->totalItemCount; ?></span></h3>
		<?php if(count($dataProvider->totalItemCount)): ?>
				<?php 
					$s = new WikiQuestion('search');
					$dataProvider=new CActiveDataProvider($s->with('user')->owner($user->id)->newest()->questions());
					$dataProvider->pagination = array('pageSize'=>5);
					$this->widget('bootstrap.widgets.TbListView', array(
						'dataProvider'=>$dataProvider,
						'itemView'=>'/question/_question',
						'template'=>"{pager}\n{summary}\n{sorter}\n{items}\n{pager}",
					));
				?>
		<?php else: ?>
			<em class="muted"><?php echo $user->first_name; ?> has not asked any questions yet! </em>
			<?php $unanswered = WikiData::countUnansweredQuestions(); ?>
			<?php if ($unanswered) : ?>
				<em class="muted">Mr No-it-all!</em> <br />
				<span class="badge badge-important"><?php echo $unanswered; ?></span> questions need answering.
				<a href="<?php echo NHtml::url('/wiki/question/unanswered'); ?>">Lend a hand answering questions</a>
			<?php endif; ?>
		<?php endif; ?>

		<!-- Your Answers -->
		<hr>
		<?php
			$s = new WikiQuestion('search');
			$dataProvider=new CActiveDataProvider($s->with('user')->owner($user->id)->newest()->answers()); 
		?>
		<h3>Answers <span class="badge"><?php echo $dataProvider->totalItemCount; ?></span></h3>
		<?php if(count($dataProvider->totalItemCount)): ?>
			<?php 
				$dataProvider->pagination = array('pageSize'=>5);
				$this->widget('bootstrap.widgets.TbListView', array(
					'dataProvider'=>$dataProvider,
					'itemView'=>'/question/_answer',
					'template'=>"{pager}\n{summary}\n{sorter}\n{items}\n{pager}",
				));
			?>
		
		<?php else: ?>
			<em class="muted"><?php echo $user->first_name; ?> has not answered any questions yet! </em>
			<?php $unanswered = WikiData::countUnansweredQuestions(); ?>
			<?php if ($unanswered == 0) : ?>
				<p>There are no questions that need answering. How about that?</p>
			<?php else: ?>
				<em class="muted">Mr take-take-take!</em> <br />
				<?php if($unanswered == 1): ?>
					There is <span class="badge badge-important"><?php echo $unanswered; ?></span> question that needs answering.
				<?php else: ?>
					<span class="badge badge-important"><?php echo $unanswered; ?></span> questions need answering.
				<?php endif; ?>
				<a href="<?php echo NHtml::url('/wiki/question/unanswered'); ?>">Lend a hand answering questions</a>
			<?php endif; ?>
		<?php endif; ?>
		
	</div>
</div>