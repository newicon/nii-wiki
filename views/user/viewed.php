<?php 
$s = new WikiView('search');
$dataProvider=new CActiveDataProvider($s, array(
	'criteria'=>array(
		'order'=>'viewed_on DESC',
		'with'=>array('user')
	)
));	
$this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_viewed',
	'template'=>"{pager}\n{summary}\n{sorter}\n{items}\n{pager}",
));
?>
