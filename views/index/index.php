<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<!--<h3>Featured</h3>
<?php //foreach(WikiPage::model()->featured()->newest()->with('author')->findAll() as $page): ?>
	
	<div class="media">
		<div class="pull-left">
			<?php //echo WikiHtml::userImage($page->author, 24); ?>
		</div>
		<div class="media-body" style="padding-bottom:2px;">
			<a href="<?php //echo NHtml::url($page->route); ?>"><?php //echo $page->title; ?></a> <br/>
			<span class="muted"><?php //echo $page->summary ?></span>
		</div>
	</div>		
	
<?php //endforeach; ?>-->

<h3>Recent Posts</h3>
<?php foreach(WikiPage::model()->publishedRecently()->with('author')->findAll() as $page): ?>
	<div class="media">
		<div class="pull-left">
			<?php echo WikiHtml::userImage($page->author, 24); ?>
		</div>
		<div class="media-body" style="padding-bottom:2px;">
			<a href="<?php echo NHtml::url($page->route); ?>"><?php echo $page->title; ?></a> <br/>
			<span class="muted"><?php echo $page->summary ?></span>
		</div>
	</div>
<?php endforeach; ?>


<h3>Recent Questions</h3>
<?php foreach(WikiQuestion::model()->questions()->findAll() as $quest): ?>
<?php $answersCount = $quest->countAnswers(); ?>
<div class="media niicomment" style="display:inline-block;">
	<div class="pull-left" style="padding-top:20px;">
		<?php echo WikiHtml::userImage($quest->user, 24); ?>
	</div>
	<div class="media-body" style="padding-bottom:2px;">
		<div style="padding-left:25px;">
			<strong><small><?php echo $quest->getUserName(); // $quest->user->name ?></small></strong>&nbsp;&nbsp; 
			&nbsp;&nbsp;
			<small class="muted"><?php echo NTime::niceShort($quest->created_at) ?></small>
		</div>
		<div class="speech-bubble commentsbox">
			<a href="<?php echo $quest->link; ?>"><?php echo $quest->title; ?></a> 
			<small class="question-status">
				<a href="<?php echo NHtml::url($quest->route); ?>">
					<?php if ($quest->countAnswers()): ?>
						<?php if ($quest->accepted_answer_id): ?>
							<span class="badge badge-success" rel="tooltip" title="Answered and accpted" data-placement="left"><?php echo $answersCount; ?></span>
						<?php else: ?>
							<span class="badge badge-warning" rel="tooltip" title="No accepted answer" data-placement="left"><?php echo $answersCount; ?></span>
						<?php endif; ?>
					<?php else: ?>
						<span class="badge badge-important" rel="tooltip" title="No answers" data-placement="left"><?php echo $answersCount; ?></span>
					<?php endif; ?>
				</a>
			</small>
			<?php echo NHtml::markdown($quest->text); ?>
		</div>
	</div>
</div><br/>
<?php endforeach; ?>
