<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<?php 
	$s = new WikiPage('search');
	$dataProvider=new CActiveDataProvider($s->newest()->published());
	$dataProvider->pagination = array('pageSize'=>10);
	$this->widget('bootstrap.widgets.TbListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_article',
		'template'=>"{pager}\n{summary}\n{sorter}\n{items}\n{pager}",
	));
?>