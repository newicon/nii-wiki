<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<hr />

<div class="media">
	<div class="media-body">
		
			<h4 class="mbn" style="font-weight:normal;margin-top:0px;"><a href="<?php echo CHtml::encode($data->link); ?>"><?php echo CHtml::encode($data->title); ?></a> <span class="badge"><?php echo $data->getViews(); ?></span></h4> 
			<p class="muted"><?php echo NHtml::previewText($data->summary, 250); ?></p>
			<?php if ($data->author !== null): ?>
				<a class="media" href="<?php echo WikiHtml::userWikiProfileUrl(($data->author ? $data->author->id : null)) ?>"><img src="<?php echo ($data->author ? $data->author->getProfileImageUrl(24) : '') ?>" class="pull-left media-object"/><div class="media-body"><?php echo ($data->author ? $data->author->name : 'unknown'); ?></div></a>
			<?php else: ?>
				<span class="muted">Unknown</span>
			<?php endif; ?>
	</div>
</div>