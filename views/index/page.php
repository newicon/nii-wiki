<?php
/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<style>
	.avatar-tab{width:40px;height:40px;background-color:#ccc;padding:60px 10px 10px 10px;;position: absolute;top:-40px;right:-70px;border-radius:0px 0px 10px 10px;}
</style>

<div class="doc-admin-panel search-bar">
	<a class="btn" href="<?php echo NHtml::url(array('wiki/index/edit', 'id' => $model->id)); ?>">Edit</a> &nbsp;&nbsp; views: <?php echo $model->getViews(); ?>
</div>
<br/>

<div class="line">
	<div class="doc" style="border-radius:0px;">
		<div class="doc-inner">
			<div class="doc-header row-fluid" style="position:relative;">
				<div class="span9" >
					<h1><?php echo $model->title; ?></h1>
					<p class="muted"><?php echo $model->summary; ?></p>
					<div class="tags" style="margin-left:-10px;">
						<?php foreach($model->tags as $tag) {echo WikiHtml::tag($tag);} ?>
					</div>
				</div>
				<div class="span3 text-right" style="padding-top:20px;">
					<small class="muted">Contributed by</small><br />
					<?php if ($model->author): ?>
						<a href="<?php echo WikiHtml::userWikiProfileUrl($model->author->id); ?>"><?php echo $model->author->name; ?></a><br/>
					<?php else: ?>
						<span class="muted">unknown</span><br/>
					<?php endif; ?>
					<?php if ($model->published): ?>
						<small class="muted">Published: <?php echo NTime::niceShort($model->published_on) ?></small>
					<?php endif; ?>
				</div>
				<div class="avatar-tab">
					<?php if ($model->author): ?>
						<a href="<?php echo WikiHtml::userWikiProfileUrl($model->author->id); ?>"><img src="<?php echo $model->author->getProfileImageUrl(50); ?>" /></a>
					<?php endif; ?>
				</div>
			</div>
			<hr />
			<div class="markdown-body">
				<?php echo NHtml::markdown($model->text); ?>
			</div>
		</div>
	</div>
</div>

<div class="line">
	<div class="unit">
		<h3>About This Post</h3>
		<table class="table">
			<?php if ($model->published_on): ?>
				<tr>
					<th>Published On:</th><td><?php echo NTime::nice($model->published_on); ?></td>
				</tr>
			<?php endif; ?>
			<?php if ($model->published_on != $model->updated_at): ?>
				<tr>
					<th>Last Updated:</th><td><?php echo NTime::timeAgoInWords($model->updated_at); ?></td>
				</tr>
			<?php endif; ?>
			<tr>
				<th>Revision:</th><td><?php echo $model->version; ?></td>
			</tr>
			<tr>
				<th>Views:</th><td><?php echo $model->getViews(); ?></td>
			</tr>
			<tr>
				<td colspan="2">
					<?php foreach(WikiView::getRecentViewers($model) as $viewer): ?>
						<div class="pull-left" style="padding-right:5px;">
							<?php
								$this->widget('user.widgets.NUserImage',array(
									'user'=>$viewer['viewed_by'],
									'size'=>32,
									'tooltipText'=>NTime::timeAgo($viewer['viewed_on'])
								)); 
							?>
						</div>
					<?php endforeach; ?>
				</td>
			</tr>
		</table>
		
		<strong>Author</strong>
		
		<?php if ($model->author): ?> 
			<?php $this->widget('wiki.widgets.WikiUser', array('user' => $model->author)); ?>
		<?php endif; ?>
	</div>
	<div class="lastUnit">
		<div style="padding-left:20px;">
			<h3>Comments</h3>
			<?php
			$this->widget('nii.widgets.comments.NiiComments', array(
				'model' => 'WikiPage',
				'id' => $model->id,
			));
			?>
		</div>
	</div>
</div>