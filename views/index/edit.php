<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<style>
	.wiki-edit{height:500px;}
	.summary{height:24px;}
	.input-title {font-size:18px;background-color:#ccc;}
</style>


<?php $form = $this->beginWidget('nii.widgets.NActiveForm', array(
	'id'=>'wiki-page',
	'action'=>$model->isNewRecord ? NHtml::url('/wiki/index/edit') : NHtml::url(array('/wiki/index/edit', 'id'=>$model->id)),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	)
)); ?>

<div class="doc-admin-panel search-bar line">
	<div class="btn-group pull-left">
		<?php if ($model->published==1): ?>
		<input name="publish" onclick="$('#WikiPage_published').val(0)" class="btn btn-primary" type=submit style="" value="Unpublish" />
		<?php else: ?>
		<input name="publish" onclick="$('#WikiPage_published').val(1)" class="btn btn-primary" type=submit style="" value="Publish" />
		<?php endif; ?>
		<?php echo $form->hiddenField($model, 'published'); ?>
		<input class="btn btn-primary" type=submit style="" value="Save" />
		<input name="SaveExit" class="btn btn-primary" type=submit style="" value="Save and Exit" />
	</div>
	<div class="pull-left mll mts">
		<?php echo $form->checkboxField($model, 'featured'); ?>
	</div>
	<a class="btn btn-link pull-right" href="<?php echo NHtml::url(array('/wiki/index/delete', 'id'=>$model->id)); ?>"><span class="text-error">Delete</span></a>
</div>
<br/>

<?php $summaryLength = 250; ?>


	<div class="doc" style="padding:60px 80px;">
		<div class="control-group">
			<?php echo $form->textField($model, 'title', array('class'=>'input-title input-block-level ','maxlength'=>255, 'style'=>'font-size:20px;height:40px;line-height:35px;', 'placeholder'=>'Title')); ?>
			<?php echo $form->error($model, 'title') ?>
		</div>
		<div class="control-group">
			<?php echo $form->textArea($model, 'summary', array('class'=>'input-block-level summary', 'placeholder'=>'Summary', 'maxlength'=>$summaryLength)); ?>
			<div class="countdown help-block man pull-right"><?php echo $summaryLength; ?> characters left.</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="token-input-WikiPage_tags" style="text-align:left;width:50px;">Tags:</label>
			<div class="lastUnit">
				<?php 
					$this->widget('nii.widgets.NTagInput', array(
						'model'=>$model,
						'attribute'=>'tags'
					)); 
				?>
			</div>
		</div>
		<div class="control-group">
			<?php echo $form->textArea($model, 'text', array('class'=>'input-block-level wiki-edit')); ?>
		</div>
	</div>



	
	
	
	
<?php $this->endWidget(); ?>

<script>
	$('.summary').focus(function(){
		$(this).animate({height: '100'}, 250)
	}).blur(function(){
		$(this).animate({height: '24'}, 500)
	}).keyup(updateCountdown).change(updateCountdown);
	
	function updateCountdown() {
		//  is the max message length
		//
		var remaining = <?php echo $summaryLength; ?> - jQuery('.summary').val().length;
		jQuery('.countdown').text(remaining + ' characters left.');
		
	}
</script>