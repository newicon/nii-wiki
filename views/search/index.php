<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>


<h3>Search Results for: "<?php echo $q; ?>"</h3>

<a href="<?php echo NHtml::url('/wiki/search/record'); ?>">View Search Stats</a>
<style>.searchHilite{font-weight:bold;}</style>

<?php  ?>
<?php if (!empty($results)): ?>
	<?php foreach($results as $result): ?>                  
		<h3 class="mbn" style="font-weight:normal;"><a href="<?php echo CHtml::encode($result->link); ?>"><?php echo NHtml::hilightText(CHtml::encode($result->title), $q); ?></a></h3>
		<cite><?php echo CHtml::link(NHtml::hilightText(CHtml::encode($result->link), $q), $result->link); ?></cite>
		<p><?php echo NHtml::hilightText(CHtml::encode($result->summary),$q); ?></p>
		<hr/>
	<?php endforeach; ?>

<?php else: ?>
	<p class="error">No results matched your search terms. <a href="<?php echo NHtml::url(array('/wiki/index/edit','title'=>$q)); ?>" class="btn btn-primary">Create a new page called "<?php echo CHtml::encode($q); ?>"</a></p>
<?php endif; ?>