<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<h1>Searches</h1>
<?php $model = WikiQuery::model()->with('user')->searchModel(); ?>
<?php $this->widget('nii.widgets.grid.NGridView', array(
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name' => 'q',
			'header'=>'Searched For',
			'type'=>'raw',
			'value' => 'CHtml::link(CHtml::encode($data->q), array("wiki/search/index","a"=>$data->q))',
		),
		array(
			'name' => 'user_id',
			'header'=>'By',
			'filter'=>false,
			'type'=>'raw',
			'value' => '$data->user->name',
		),
		array(
			'name' => 'created_at',
			'header'=>'When',
			'type'=>'raw',
			'filter'=>false,
			'value' => 'NTime::niceShort($data->created_at)',
		),
	),
)); ?>

<h2>Frequent Searches</h2>
<?php foreach(WikiQuery::searchFrequency() as $q): ?>
<div>
	<?php echo $q['freq'];?> x <a href="<?php echo NHtml::url(array('/wiki/search/index', 'q'=>$q['q'])) ?>"><?php echo $q['q'];?></a>
</div>
<?php endforeach; ?>
