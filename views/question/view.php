<?php
/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<?php
/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<style>
	.hidden {visibility:hidden;}
	.speech-bubble:hover .hidden{visibility:visible;}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.5/angular.min.js"></script>
<div ng-app ng-controller="Quest.questionCtrl">
	
	<div class="media niicomment" ng-cloak>
		<a class="pull-left" href="#">
			<img class="user-thumb" ng-src="{{question.user_image}}" />
		</a>
		<div class="media-body" style="padding-bottom:2px;" >
			<div class="speech-bubble commentsbox">
				<div class="row-fluid">
					<div class="span9"><strong>{{question.user_name}}</strong></div>
					<div class="span3 pull-right text-right"><small class="muted">{{question.time_ago}}</small></div>
				</div>
				<h3 class="mtn">{{question.title}}</h3>
				<div ng-bind-html-unsafe="question.text"></div>

				<div class="control-group row">
					<a href="#" data-id="46" class="pull-right text-small delete hidden"><small class="text-error">Delete</small></a>
					<a href="{{question.url_edit}}" data-id="46" class="pull-right text-small hidden" style="margin-right:5px;"><small>Edit</small></a>
				</div>
			</div>
			<div style="padding-left:15px;">
				<span ng-repeat="tag in tags">
					<span ng-bind-html-unsafe="tag"></span>
				</span>
				&nbsp;views: {{views}}
			</div>
		</div>
	</div>
	<h3 ng-cloak>{{answers.length}} Answers</h3>
	<hr />
	<div class="media niicomment" ng-repeat="answer in answers" ng-cloak>
		<div class="pull-left" href="#">
			<img class="user-thumb thumbnail" ng-src="{{answer.user_image}}" class="thumbnail user-thumbnail" />
			<div ng-switch on="isAccepted(answer)" >
				<span ng-switch-when="true" class="label label-success">Accepted</span>
				<span ng-switch-default><a ng-hide="!canAccept()" ng-click="acceptAnswer(answer)" class="btn">Accept</a></span>
			</div>
		</div>
		<div class="media-body" style="padding-bottom:2px;">
			<div class="speech-bubble commentsbox">
				<div class="row-fluid">
					<div class="span9"><strong>{{answer.user_name}}</strong></div>
					<div class="span3 pull-right text-right"><small class="muted">{{answer.time_ago}}</small></div>
				</div>
				<div ng-bind-html-unsafe="answer.text"></div>
				<div class="control-group row" >
					<a ng-hide="!answer.id" ng-click="deleteAnswer($index)" href="" data-id="46" class="pull-right text-small delete hidden"><small class="text-error">Delete</small></a>
				</div>
			</div>
		</div>
	</div>
	
	<div class="media" ng-cloak>
		<a class="pull-left" href="#">
			<img class="user-thumb thumbnail" ng-src="{{user_image}}" class="thumbnail user-thumbnail" />
		</a>
		<div class="media-body" style="padding-bottom:2px;">
			<div class="speech-bubble">
				<form name="answerForm">
					<div class="control-group">
						<textarea ng-model="answerText" name="WikiAnswer[text]" class="input-block-level" placeholder="Answer" style="height:200px"></textarea>
					</div>
					<div class="form-actions">
						<button ng-click="saveAnswer(answer)" ng-disabled="answerForm.$invalid" type="submit" class="btn btn-primary pull-right" data-loading-text="Loading...">{{answerSubmitText}}</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php if (count($q->tags) >= 1): ?>
	Browse other questions tagged
	<?php foreach ($q->tags as $tag) : ?>
		<?php echo WikiHtml::tag($tag); ?>
	<?php endforeach; ?>
	or 
<?php endif; ?>
<a class="btn " href="<?php echo NHtml::url('/wiki/question/ask'); ?>">Ask Your Own Question</a>

<?php $user = Yii::app()->user->record; ?>
<script type="text/javascript">
	
	var Quest = {
		question_id:<?php echo $q->id ?>,
		owner_id: <?php echo $q->owner_id; ?>,
		user_id: <?php echo $user->id; ?>,
		answerObj : {
			id:null,
			question_id:<?php echo $q->id; ?>,
			user_name:<?php echo json_encode($user->name) ?>, 
			user_image:<?php echo json_encode($user->getProfileImageUrl(64)); ?>,
			owner_id:<?php echo $user->id; ?>
		},
		questionCtrl:function($scope){
			$scope.views = <?php echo $q->getViews(); ?>;
			$scope.question = <?php echo $q->toJson(); ?>;
			$scope.tags = <?php echo WikiHtml::tagsToJson($q->tags); ?>;
			$scope.answers = <?php echo WikiData::getJsonQuestionAnswers($q); ?>;
			$scope.accepted_answer_id = <?php echo json_encode($q->accepted_answer_id) ?>;
			
			// delete answer
			$scope.deleteAnswer = function(index){
				if (confirm('Are you sure you want to delete this answer?')) {
					var todelete = $scope.answers[index];
					$scope.answers.splice(index, 1);
					var url = "<?php echo NHtml::url(array('/wiki/question/answerDelete')); ?>/id/"+todelete.id;
					$.post(url);
				}
			}
			
			// accept an answer
			$scope.acceptAnswer = function(answer){
				$scope.accepted_answer_id = answer.id;
				var url = "<?php echo NHtml::url(array('/wiki/question/answerAccept')); ?>";
				$.post(url, {question:Quest.question_id, answer:answer.id});
			}
			
			$scope.isAccepted = function(answer){
				return (answer.id == $scope.accepted_answer_id)
			}

			$scope.canAccept = function(){
				return (Quest.owner_id == Quest.user_id);
			}

			// adding an answer
			$scope.user_image = Quest.answerObj.user_image;
			$scope.answerText = '';
			$scope.answerSubmitText = 'Answer';
			$scope.saveAnswer = function(answer){
				$scope.answerSubmitText = 'Saving...';
				var url = "<?php echo NHtml::url(array('/wiki/question/answer', 'id'=>$q->id)); ?>";
				var answer = {text:$scope.answerText};
				angular.extend(answer, Quest.answerObj);
				$scope.answerText = '';
				$scope.answers.push(answer)
				$scope.answerSubmitText = 'Loading...';
				$.post(url, {WikiAnswer:answer}, function(json){
					angular.extend(answer, json);
					$scope.answerSubmitText = 'Answer';
					$scope.$apply();
				},'json');
			};
		}
	}
</script>

