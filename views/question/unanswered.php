<?php

/**
 * Nii view file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<?php $this->renderPartial('_header'); ?>

<?php 
	$s = new WikiQuestion('search');
	$dataProvider=new CActiveDataProvider($s->with('user')->unanswered());
	$this->widget('bootstrap.widgets.TbListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_question',
));
?>