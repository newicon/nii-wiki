<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<ul class="nav nav-pills">
	<li class="<?php echo NHtml::active('/wiki/question/index') ?>">
		<a href="<?php echo NHtml::url('/wiki/question/index') ?>">Newest</a>
	</li>
	<li class="<?php echo NHtml::active('/wiki/question/unanswered') ?>">
		<a href="<?php echo NHtml::url('/wiki/question/unanswered') ?>">Unanswered <?php echo WikiHtml::unansweredBadge(); ?></a>
	</li>
</ul>
