<?php $this->renderPartial('_header'); ?>

<?php 
	$s = new WikiQuestion('search');
	$dataProvider=new CActiveDataProvider($s->with('user')->newest()->questions());
	if ($dataProvider->itemCount) {
		$this->widget('bootstrap.widgets.TbListView', array(
			'dataProvider'=>$dataProvider,
			'itemView'=>'_question',
			'template'=>"{pager}\n{summary}\n{sorter}\n{items}\n{pager}",
		));
	} else {
		echo 'No Question yet... <a class="btn btn-primary" href="'.NHtml::url('wiki/question/ask').'">Ask the first question!</a>';
	}
?>