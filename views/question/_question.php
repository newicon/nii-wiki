<?php
/**
 * Wiki view file
 * This is a template view for use with CListView widget the $data variable is the 
 * WikiQuestion record representing a question
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>


<hr />
<?php $answersCount = $data->countAnswers(); ?>
<div class="media niicomment">
	<div class="pull-left" style="padding-top:20px;">
		<?php echo WikiHtml::userImage($data->user, 72); ?>
		
		<div class="question-status ptm">
			<a href="<?php echo NHtml::url($data->route); ?>">
			<?php if ($answersCount): ?>
				<?php if ($data->accepted_answer_id): ?>
					<span class="badge badge-success" rel="tooltip" title="Answered and accpted" data-placement="left"><?php echo $answersCount; ?> Answers</span>
				<?php else: ?>
					<span class="badge badge-warning" rel="tooltip" title="No accepted answer" data-placement="left"><?php echo $answersCount; ?> Answers</span>
				<?php endif; ?>
			<?php else: ?>
				<span class="badge badge-important" rel="tooltip" title="No answers" data-placement="left"><?php echo $answersCount; ?> Answers</span>
			<?php endif; ?>
			</a>
		</div>
	</div>
	<div class="media-body" style="padding-bottom:2px;">
		<div style="padding-left:25px;">
			<strong><small><?php echo $data->user->name ?></small></strong>&nbsp;&nbsp; <small class="muted;"><?php echo NTime::niceShort($data->created_at) ?></small>
		</div>
		<div class="speech-bubble commentsbox">
			<h4 class="mtn"><a href="<?php echo NHtml::url($data->getRoute()) ?>" ><?php echo $data->title; ?></a></h4>
			<?php echo NHtml::markdown($data->text); ?>

			<div style="margin-left:-10px;">
			<?php foreach(WikiData::getTags($data) as $tag): ?>
				<?php echo WikiHtml::tag($tag); ?>
			<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>

