<?php

/**
 * Wiki view file
 * This is a template view for use with CListView widget the $data variable is the 
 * WikiQuestion record representing an answer
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<?php if ($data->getQuestion() === null): ?>
<?php echo $data->question_id; ?>
<?php echo $data->text; ?>
Orphaned Answer <?php echo $data->id; ?>
<?php else: ?>
<hr />
<div class="media niicomment">
	<div class="pull-left ptl">
		<?php echo WikiHtml::userImage($data->user, 48); ?>
		
		<div class="question-status ptm">
			<a href="<?php echo NHtml::url($data->question->route); ?>">
				<?php if ($data->question->accepted_answer_id): ?>
					<?php if ($data->id == $data->question->accepted_answer_id): ?>
						<span class="badge badge-success" data-placement="left">Accepted</span>
					<?php else: ?>
						<span class="badge" rel="tooltip" title="A different answer was accepted" data-placement="left">Not Quite</span>
					<?php endif; ?>
				<?php else: ?>
					<!-- No accepted answer for this question yet -->
					<span class="badge" rel="tooltip" title="No accepted answer" data-placement="left">Pending</span>
				<?php endif; ?>
			</a>
		</div>
	</div>
	<div class="media-body" style="padding-bottom:2px;">
		<div style="padding-left:25px;">
			<strong><small><?php echo $data->user->name ?></small></strong> &nbsp; &nbsp; <small style="color:#999;"><?php echo NTime::niceShort($data->created_at) ?></small>
		</div>
		<div class="speech-bubble commentsbox">
			Answer To: <a href="<?php echo NHtml::url($data->question->route); ?>"><?php echo $data->question->title; ?></a>
			<h4 class="mtn"><a href="<?php echo NHtml::url($data->getRoute()) ?>" ><?php echo $data->title; ?></a></h4>

			<?php echo NHtml::markdown(NHtml::previewText($data->text, 150,  array('append'=>'...'))); ?>

			<div style="margin-left:-10px;">
			<?php foreach(WikiData::getTags($data) as $tag): ?>
				<?php echo WikiHtml::tag($tag); ?>
			<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

