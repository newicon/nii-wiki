<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<div class="media">
  <a class="pull-left" href="#">
	<?php $this->widget('user.widgets.NUserImage',array('size'=>64, 'htmlOptions'=>array('class'=>'media-object thumbnail user-thumbnail'))); ?>
  </a>
	<div class="media-body" style="padding-bottom:2px;">
		<div class="speech-bubble">
			<?php $form = $this->beginWidget('nii.widgets.NActiveForm', array(
				'id'=>'wiki-question', 
				'action'=>$model->isNewRecord ? NHtml::url('/wiki/question/ask') : NHtml::url(array('/wiki/question/ask', 'id'=>$model->id)),
				'htmlOptions'=>array('style'=>'margin:0px;')
			)) ?>
			<div class="control-group">
				<?php echo $form->textField($model, 'title', array('class'=>'input-block-level', 'placeholder'=>"What's your question? Be specific.", 'style'=>'font-size:18px;height:35px;', 'rows'=>3)) ?>
				<?php echo $form->error($model, 'title'); ?>
			</div>
			<div class="control-group">
				<?php echo $form->textArea($model, 'text', array('class'=>'input-block-level', 'placeholder'=>"Detailed info", 'style'=>'height:200px')) ?>
				<?php echo $form->error($model, 'text'); ?>
			</div>
			<div class="control-group">
				<label for="token-input-WikiQuestion_tags">Tags:
					<?php 
						$this->widget('nii.widgets.NTagInput', array(
							'model'=>$model,
							'attribute'=>'tags'
						)); 
					?>
				</label>
			</div>
			
			<div class="form-actions">
				<button type="submit" class="btn btn-primary pull-right">Post Your Question</button>
			</div>
			
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>

<script>
$(function(){
	$('#WikiQuestion_title').focus();
});
</script>