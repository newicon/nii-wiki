<?php

/**
 * WikiData class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of WikiData
 *
 * @author 
 */
class WikiData
{
	/**
	 * get draft posts for this user, if $userId is null then the current 
	 * session user is assumed Yii::app()->user
	 * @param int $userId
	 */
	public static function getMyDraftPosts($userId=null)
	{
		return WikiPage::model()->author($userId)->drafts()->findAll();
	}
	
	public static function getMyTrashedPosts($userId=null)
	{
		return WikiPage::model()->author($userId)->trashed()->findAll();
	}
	
	public static function getMyQuestions()
	{
		return WikiQuestion::model()->my()->questions()->findAll();
	}
	
	public static function getMyAnswers()
	{
		return WikiQuestion::model()->my()->answers()->findAll();
	}
	
	public static function getUserPostsPublished($userId=null)
	{
		return WikiPage::model()->author($userId)->findAll();
	}
	
	public static function getUserQuestions($userId=null)
	{
		return WikiQuestion::model()->owner($userId)->questions()->findAll();
	}
	
	public static function getUserAnswers($userId=null)
	{
		return WikiQuestion::model()->owner($userId)->answers()->findAll();
	}
	
	/**
	 * Get all answers as a json array for the question
	 * @param WikiQuestion $question
	 * @return strnig json
	 */
	public static function getJsonQuestionAnswers(WikiQuestion $question)
	{
		return CJSON::encode($question->getAnswers());
	}
	
	private static $_countUnansweredQuestions;
	
	public static function countUnansweredQuestions()
	{
		if (self::$_countUnansweredQuestions === null) {
			self::$_countUnansweredQuestions = WikiQuestion::model()->unanswered()->count();
		}
		return self::$_countUnansweredQuestions;
	}
	
	/**
	 * get tags from a model with a taggable behavior
	 * enables caching when data gets big
	 * @param type $model
	 */
	public static function getTags($model)
	{
		return $model->getTags();
	}
}