<?php

/**
 * WikiUser class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of WikiUser
 * Wiki User behavior user specific functions to extend NWebUser for the wiki
 * // attach an achiive behavior to the user
 * Yii::app()->user->attachBehavior('wikiuser', array('class'=>'achiive.components.WikiUser'));
 */
class WikiUserBehavior extends CBehavior
{
	public function followers()
	{
		return WikiFollow::getFollowers();
	}
	
	public function following()
	{
		return WikiFollow::getFollowing();
	}
}