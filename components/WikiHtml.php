<?php

/**
 * WikiHtml class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * WikiHtml provides helper functions to render HTML elements n ting
 */
class WikiHtml
{
	/**
	 * render a html tag
	 * @param string $tag tag name
	 */
	public static function tag($tag)
	{
		return '<a class="btn btn-link" href="'.NHtml::url(array('/wiki/search/index','q'=>'tag:('.$tag.')')).'"><i class="icon-tag prs" style="opacity:0.5"></i>'.$tag.'</a>';
	}
	
	/**
	 * takes an array of tags and converts to an json array
	 * encoding each tag to html using self::tag()
	 * @param array $tags
	 * @return json
	 */
	public static function tagsToJson($tags)
	{
		$arr = array();
		foreach($tags as $tag){
			$arr[] = self::tag($tag);
		}
		return json_encode($arr);
	}
	
	/**
	 * Display a user image
	 * @param User $user
	 * @param int $size size of the image
	 */
	public static function userImage($user, $size=40)
	{
		$id = isset($user->id) ? $user->id : null;
		if ($id == null)
			return '?';
		return '<a href="'.NHtml::url(array('wiki/user/profile', 'id'=>$user->id)).'">'.$user->getProfileImage($size).'</a>';
	}
	
	/**
	 * return the url to the user wiki profile page
	 * @param int $userId user id
	 * @return string html
	 */
	public static function userWikiProfileUrl($userId)
	{
		return NHtml::url(self::userWikiProfileRoute($userId));
	}
	
	/**
	 * Wiki user profile route
	 * @param int $userId
	 * @return array
	 */
	public static function userWikiProfileRoute($userId)
	{
		return array('wiki/user/profile', 'id'=>$userId);
	}
	
	/**
	 * Draw a html badge element with the number of unanswered questions
	 * If there are no 
	 */
	public static function unansweredBadge()
	{
		return WikiData::countUnansweredQuestions() ? '<span class="badge badge-important">' . WikiData::countUnansweredQuestions() . '</span>' : '';
	}
}