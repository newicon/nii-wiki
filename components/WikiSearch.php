<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

require_once('Zend/Search/Lucene.php');
require_once('StandardAnalyzer/Analyzer/Standard/English.php');

/**
 * helper class to aid Zend search lucene on the wiki
 *
 * @author steve
 */
class WikiSearch extends CComponent {
	
	/**
	 * store singleton object
	 * @var WikiSearch
	 */
	private static $_self;
	
	private function __construct(){}
	
	/**
	 * get WikiSearch static instance
	 * @return WikiSearch
	 */
	public static function getInstance()
	{
		if(self::$_self===null)
			self::$_self = new WikiSearch();
		return self::$_self;
	}
	
	public function getIndexDirectory()
	{
		return Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'wiki'.DIRECTORY_SEPARATOR.'search';
	}
	
	/**
	 * @see self::getInstance();
	 * @return WikiSearch
	 */
	public static function get()
	{
		return self::getInstance();
	}
	
	/**
	 * find something in the index by $query terms
	 * @param string $query string query search terms
	 * @return array search results
	 */
	public function find($query)
	{
		$query = Zend_Search_Lucene_Search_QueryParser::parse($query);
        return self::get()->openIndex()->find($query);
	}
	
	/**
	 * Open an existing index
	 * @return Zend_Search_Lucene
	 */
	public function openIndex()
	{
		Zend_Search_Lucene_Analysis_Analyzer::setDefault(new StandardAnalyzer_Analyzer_Standard_English);
		return Zend_Search_Lucene::open($this->indexDirectory);
	}
	
	/**
	 * Builds the zend lucene index
	 */
	public function buildIndex()
	{
		Zend_Search_Lucene_Analysis_Analyzer::setDefault(new StandardAnalyzer_Analyzer_Standard_English);
		$index = new Zend_Search_Lucene($this->indexDirectory, true);
		
        foreach (WikiPage::model()->findAll() as $post)
            $index->addDocument($post->luceneGetDocument());
		foreach (WikiQuestion::model()->findAll() as $quest)
            $index->addDocument($quest->luceneGetDocument());
		
		
		
		// lets index all the tasks in the system :-)
//		$this->flushStart();
//		foreach(ProjectTask::model()->findAll() as $t){
//			$this->flushMsg($t->id);
//			usleep(500);
//			$doc = new Zend_Search_Lucene_Document();
//			$doc->addField(Zend_Search_Lucene_Field::Keyword('ProjectTask_id', $t->id));
//			$doc->addField(Zend_Search_Lucene_Field::Keyword('model', 'ProjectTask'));
//			$doc->addField(Zend_Search_Lucene_Field::unIndexed('pk', $t->id));
//			$doc->addField(Zend_Search_Lucene_Field::Text('title', CHtml::encode($t->name), 'utf-8'));
//			$doc->addField(Zend_Search_Lucene_Field::Text('link', CHtml::encode($t->link), 'utf-8'));
//			$doc->addField(Zend_Search_Lucene_Field::Text('author', $t->created_by));
//			$doc->addField(Zend_Search_Lucene_Field::Text('summary', substr($t->description, 0, 155), 'utf-8'));
//			// index the content but don't bother storing it
//			$doc->addField(Zend_Search_Lucene_Field::unStored('text', $t->description, 'utf-8'));
//
//			$index->addDocument($doc);
//		}
//		
//		$this->flushMsg('Optimizing');
		$index->optimize();
	
        $index->commit();
	}
	
	function flush ()
	{
		echo(str_repeat(' ',256));
		// check that buffer is actually set before flushing
		if (ob_get_length()){            
			@ob_flush();
			@flush();
			@ob_end_flush();
		}    
		@ob_start();
	}
	
	public function flushStart()
	{
		// turn off logging to prevent unknown exception stack error
		FB::setEnabled(false);
		// dumb hack for ie. will not flush buffer until over 256 characters.
		echo str_repeat(' ', 256);
		echo '<br/>';
	}
	
	public function flushMsg($msg) 
	{
		echo "$msg<br/>";
		echo '<script>';
		echo ' window.scrollBy(0,50);';
		echo '</script>';
		ob_flush();flush();
	}
	
}
