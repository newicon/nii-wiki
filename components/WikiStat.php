<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Helper class to display wiki stats
 * WikiStat::statUserPosts()
 * All of these stats probably need to be cached to keep everything fast.
 */
class WikiStat 
{
	public static function statUserPosts($userId)
	{
		return WikiPage::model()->countByAttributes(array('author_id'=>$userId));
	}
	public static function statUserQuestions($userId)
	{
		return WikiQuestion::model()->countByAttributes(array(
			'owner_id'=>$userId, 'type'=> WikiQuestion::TYPE_QUESTION));
	}
	public static function statUserAnswers($userId)
	{
		return WikiQuestion::model()->countByAttributes(array(
			'owner_id'=>$userId, 'type'=> WikiQuestion::TYPE_ANSWER));
	}
	public static function statUserAnswersAccepted($userId)
	{
		return WikiQuestion::model()->countByAttributes(array(
			'owner_id'=>$userId, 
			'type'=> WikiQuestion::TYPE_ANSWER,
			'accepted'=>1
		));
	}
	public static function statTotalComments($userId)
	{
		Yii::import('nii.widgets.comments.models.NiiComment');
		return NiiComment::model()->countByAttributes(array('user_id'=>$userId));
	}
	public static function statTotalServed($userId)
	{
		return WikiView::getTotalServed($userId);
	}
	
	/**
	 * get the user who has served the most people
	 * returns array [user_id]=>int total served
	 * @return array id=>served score
	 */
	public static function bestUser()
	{
		$totalServed = self::bestUsers();
		// reset internal array pointer to the first element
		reset($totalServed);
		// return the id of the best user
		$ret = array(key($totalServed) => current($totalServed));
		return $ret;
	}
	
	/**
	 * Get a list ordered from best to worst in terms of user who
	 * has served the most users
	 * @return array id=>served score
	 */
	public static function bestUsers()
	{
		$totalServed = array();
		foreach(User::model()->findAll() as $user){
			$totalServed[$user->id] = self::statTotalServed($user->id);
		}
		// order array from best to worst array
		arsort($totalServed);
		return $totalServed;
	}
	
	public static function displayUserStat($userId)
	{
		$posts = WikiStat::statUserPosts($userId);
		$questions = WikiStat::statUserQuestions($userId);
		$answers = WikiStat::statUserAnswers($userId);
		$comments = WikiStat::statTotalComments($userId);
		return "
			<span class=\"badge\">$posts</span> Posts <br/>
			<span class=\"badge\">$questions</span> Questions <br/>
			<span class=\"badge\">$answers</span> Answers<br/>
			<span class=\"badge\">$comments</span> Comments <br/>
		";
	}
}
