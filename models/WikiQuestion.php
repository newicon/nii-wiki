<?php

/**
 * WikiQuestion class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Table of WikiQuestion records
 * A WikiQuestion represents both a question and an answer
 * A question record will have its type defined as TYPE_QUESTION
 * An answer will obviously then have a type of TYPE_ANSWER
 * If the record is of type question, then the accepted_answer_id is the WikiQuestion
 * record that answers this question and has been accepted by the author of the question
 * All answer records will maintain a 'question_id' record that points to their parent question record
 * *simples*
 */
class WikiQuestion extends NActiveRecord
{
	
	const TYPE_QUESTION = 'TYPE_QUESTION'; 
	const TYPE_ANSWER = 'TYPE_ANSWER'; 
	
	public function tableName()
	{
		return '{{wiki_question}}';
	}
	
	/**
	 * get static model instance
	 * @param string $className
	 * @return WikiQuestion
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * core CActiveRecord Rules function
	 * @see CActiveRecord::rules
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('title, text', 'required'),
			array('question_id', 'safe')
		);
	}
	
	/**
	 * before saving we want to record the user id as this is the "owner" of the question
	 * @return type
	 */
	public function beforeSave()
	{
		if ($this->isNewRecord)
			$this->owner_id = Yii::app()->user->id;
		return parent::beforeSave();
	}
	
	/**
	 * @see CActiveRecord::behaviors
	 * @return array
	 */
	public function behaviors()
	{
		return array(
			'timestampable'=>array(
				'class'=>'nii.components.behaviors.NTimestampable'
			),
			'tagable'=>array(
               'class'=>'nii.components.behaviors.NTaggable'
            ),
			'trashable'=>array(
				'class'=>'nii.components.behaviors.NTrashBinBehavior',
			),
		);
	}
	
	/**
	 * define scopes.
	 * my = questions or answers created by the currently logged in user
	 * question = only select questions
	 * answers = only select answers
	 */
	public function scopes()
	{
		return array(
			'newest'=>array(
				'order'=>$this->getTableAlias().'.created_at desc'
			),
			'my'=>array(
				'condition'=>'owner_id='.Yii::app()->user->id,
			),
			'questions'=>array(
				'condition'=>"type='".self::TYPE_QUESTION."'"
			),
			'answers'=>array(
				'condition'=>"type='".self::TYPE_ANSWER."'"
			),
			'unanswered'=>array(
				'condition'=>'type=\''.self::TYPE_QUESTION.'\''
				. ' and NOT EXISTS (SELECT id FROM wiki_question where question_id = '.$this->getTableAlias().'.id)'
			)
		);
	}
	
	/**
	 * Limit results by owner id, 
	 * if an owner id is not specified $userId will default to the current loggedin user id
	 * @param int $authorId
	 * @return WikiPage
	 */
	public function owner($userId=null)
	{
		if ($userId===null)
			$userId = Yii::app()->user->id;
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'owner_id = '.$userId,
		));
		return $this;
	}
	
	/**
	 * Add relations namely the user relation to get the author of this question
	 * @return array
	 */
	public function relations()
	{
		return array(
			'user'=>array(self::BELONGS_TO, 'User', 'owner_id'),
			'relatedAnswers'=>array(self::HAS_MANY, 'WikiAnswer', 'question_id'),
			'question'=>array(self::BELONGS_TO, 'WikiQuestion', 'question_id')
		);
	}
	
	/**
	 * Core NActiveRecord function
	 * @see NActiveRecord::schema
	 * @return array
	 */
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				'type'=>"enum('".self::TYPE_QUESTION."','".self::TYPE_ANSWER."') NOT NULL DEFAULT '".self::TYPE_QUESTION."'",
				'title'=>'string',
				'text'=>'text',
				'owner_id'=>'int',
				'accepted_answer_id'=>'int',
				// if this is an answer then this 
				'accepted'=>'bool',
				// when the record represents an answer this is the id of its question
				'question_id'=>'int'
			),
			'keys'=>array(
				array('question_id'),
				array('accepted_answer_id')
			)
		);
	}
	
	/**
	 * Set an answer as the offical accepted answer
	 * @param int $answerId
	 */
	public function setAcceptedAnswer($answerId)
	{
		$answer = NData::load('WikiAnswer', $answerId, 'Unable to find answer with id '. $answerId);
		// reset all answers to unaccepted
		WikiAnswer::model()->updateAll(array('accepted'=>0), 'question_id = '.$answer->question_id);
		// set the answer to accepted
		$this->accepted_answer_id = $answer->id;
		$answer->accepted = 1;
		$answer->save();
		$this->save();
	}
	
	/**
	 * TODO: move into search behavior
	 */
	public function afterSave()
	{
		parent::afterSave();
		Yii::import('wiki.vendor.*');
		require_once('Zend/Search/Lucene.php');
		$this->luceneIndexUpdate();
	}
	
	/**
	 * Get the html link url for this record
	 * @see self::route
	 */
	public function getLink()
	{
		return NHtml::url($this->route);
	}
	
	/**
	 * Get the route to this record
	 * @return array
	 */
	public function getRoute()
	{
		return array('/wiki/question/view', 'id'=>$this->id);
	}
	
	/**
	 * Get the route to the edit page
	 * @return array
	 */
	public function getRouteEdit()
	{
		return array('/wiki/question/ask', 'id'=>$this->id);
	}
	
	/**
	 * store a reference to the question record if the current record is an answer
	 * @see self::getQuestion()
	 * @var WikiQuestion
	 */
	private $_question;
	
	/**
	 * if this record represents an answer then:
	 * this function gets the question this record answers
	 * @return WikiQuestion
	 */
	public function getQuestion()
	{
		if ($this->type == self::TYPE_QUESTION)
			return $this;
		
		$this->_question = WikiQuestion::model()->findByPk($this->question_id);
		return $this->_question;
	}
	
	/**
	 * get all answers for this question
	 * @return array WikiAnswer models
	 */
	public function getAnswers()
	{
		return WikiAnswer::model()->findAllByAttributes(array('question_id'=>$this->id));
	}
	
	/**
	 * count the number of answers this question has
	 * @return int
	 */
	public function countAnswers()
	{
		//TODO add cache only needs to be calculated once after the cache is cleared, increment each time
		// and answer is added / removed
		return WikiQuestion::model()->answers()->countByAttributes(array('question_id'=>$this->id));
	}

	/**
	 * return number of questions a user has created
	 * @param int $userId
	 * @return int
	 */
	public static function statUserQuestions($userId)
	{
		return WikiQuestion::model()->countByAttributes(array('type'=>self::TYPE_QUESTION, 'owner_id'=>$userId, 'trashed'=>0));
	}
	
	/**
	 * return the number of answers the user has created
	 * @param int $userId
	 * @return int
	 */
	public static function statUserAnswers($userId)
	{
		return WikiQuestion::model()->countByAttributes(array('type'=>self::TYPE_ANSWER, 'owner_id'=>$userId, 'trashed'=>0));
	}
	
	/**
	 * Override the default behavior trash functionality, 
	 * we also need to trash any answers relating to this question
	 * @return void
	 */
	public function trash()
	{
		// call parent trash function
		$this->trashable->trash();
		foreach($this->getAnswers() as $answer)
			$answer->trash();
	}
	
	/**
	 * Override the default behavior trash restore functionality, 
	 * we also need to restore any answers relating to this question
	 * @return void
	 */
	public function restore()
	{
		$this->trashable->restore();
		$answers = WikiAnswer::model()->withTrashed()->findAllByAttributes(
			array('question_id'=>$this->id));
		foreach($answers as $answer)
			$answer->restore();
	}
	
	/**
	 * return the number of views this question has had
	 * @return int
	 */
	public function getViews()
	{
		return WikiView::getViewCount($this);
	}
	
	public function toJson()
	{
		return json_encode($this->getAttributes());
	}
	
	public function getUserName()
	{
		return ($this->user) ? $this->user->name : 'unknown';
	}
	
	public function getAttributes($names=true)
	{
		return array_merge(parent::getAttributes(), array(
			'text'=>NHtml::markdown($this->text),
			'time_ago'=>NTime::timeAgoInWords($this->created_at),
			'user_image'=> ($this->user) ? $this->user->getProfileImageUrl(64) : '',
			'user_name'=>$this->getUserName(),
			'user_id'=>($this->user) ? $this->user->id : null,
			'url_edit'=>NHtml::url($this->getRouteEdit())
		));
	}
	
	// lucene stuff
	///////////////////////
	
	public function luceneLookupField()
	{
		return get_class($this) . '_id';
	}
	
	public function luceneIndexUpdate()
	{
		$index = WikiSearch::get()->openIndex();
		 // find the document based on the indexed document_id field
		$term = new Zend_Search_Lucene_Index_Term($this->id, $this->luceneLookupField());
		foreach ($index->termDocs($term) as $id)
			$index->delete($id);

		// re-add the document
		$index->addDocument($this->luceneGetDocument());

		// write the index to disk
		$index->commit();
	}
	
	public function luceneGetDocument()
	{
		$doc = new Zend_Search_Lucene_Document();
		$doc->addField(Zend_Search_Lucene_Field::Keyword($this->luceneLookupField(), $this->id));
		$doc->addField(Zend_Search_Lucene_Field::Keyword('model', 'WikiQuestion'));
		$doc->addField(Zend_Search_Lucene_Field::unIndexed('pk', CHtml::encode($this->id), 'utf-8'));
		$doc->addField(Zend_Search_Lucene_Field::Text('title', CHtml::encode($this->title), 'utf-8'));
		$doc->addField(Zend_Search_Lucene_Field::Text('link', CHtml::encode($this->link), 'utf-8'));
		$doc->addField(Zend_Search_Lucene_Field::unStored('text', CHtml::encode($this->text), 'utf-8'));
		// dont bother indexing this summary as it is a copy of the text
		$doc->addField(Zend_Search_Lucene_Field::unIndexed('summary', substr($this->text, 0, 155), 'utf-8'));
		$tags = $this->tags;
		$doc->addField(Zend_Search_Lucene_Field::Text('tag', CHtml::encode(implode(',',$tags)), 'utf-8'));
		return $doc;
	}
	
}