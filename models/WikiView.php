<?php
/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of WikiViews
 * WikiView table records the activity of a user on the wiki
 * mainly its purpose is to record the number of times a user has viewed a wiki article / question
 * and importantly what they viewed when
 * Knowing when an article was last viewed allows us to highlight the changes
 * that have been made since and any new content or comments.
 * Stats based on this allow us to determine popular articles and questions which
 * will be useful for the system to promote.
 * Also as a management / stats features for larger companies they may want to see individual user activity
 */
class WikiView extends NActiveRecord 
{
	public function tableName()
	{
		return '{{wiki_view}}';
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				'model'=>'string',
				'model_id'=>'int',
				'model_author'=>'int',
				'viewed_by'=>'int',
				'viewed_on'=>'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
			),
			'keys'=>array(
				array('model'),
				array('model_id'),
				array('model_author'),
				array('viewed_by'),
			)
		);
	}
	
	public function relations() 
	{
		return array(
			'user'=>array(self::BELONGS_TO, 'User', 'viewed_by')
		);
	}
	
	public function rules()
	{
		return array(
			array('model_author, model, model_id, viewed_by', 'safe')
		);
	}
	
	/**
	 * Log something as viewed
	 * @param CActiveRecord $model
	 */
	public static function logViewed($model, $modelAuthorId=0)
	{
		self::modelInfo($model, $className, $pk, $cacheId);
		$userId = Yii::app()->user->id;
		$attributes = array(
			'model_author'=>$modelAuthorId,
			'model'=>$className,
			'model_id'=>$pk,
			'viewed_by'=>$userId
		);
		// Check there are no records with duplicate data less than $timeTollMinutes old
		// When recording logs we do not want to add multiple view logs caused by rapid refresh.
		// using a time tollerance means we only add a log if the page is visited again,
		// after a certain expirey time. 
		$timeTollMins = '5';
		$timeAgo = date('Y-m-d H:i:s', strtotime("$timeTollMins minutes ago"));
		$duplicate = WikiView::model()->findByAttributes($attributes, "viewed_on > '$timeAgo'");
		// only add a log if we do not find a duplicate and it has been $timeTollMins
		// minutes since the last visit
		if ($duplicate===null) {
			$view = new WikiView;
			$view->attributes = $attributes;
			$view->save();
			// We could delete the cache here to trigger a query on the next page load
			// of this model, but we may as well simply update the cache ourselves manually
			$count = Yii::app()->cache->get($cacheId);
			Yii::app()->cache->set($cacheId, $count+1);
			// The author of this model is serving someone content,
			// so increment her served count. Only increment if she is serving content
			// to someone else
			if ($modelAuthorId !== 0 && $modelAuthorId != Yii::app()->user->id) {
				$servedCacheId = self::getServedCacheId($modelAuthorId);
				// An item by an author has been viewed, so update his "served" count
				$served = Yii::app()->cache->get($servedCacheId);
				Yii::app()->cache->set($servedCacheId, $served+1);
			}
		}
	}
	
	public static function getViewCount($model)
	{
		Yii::beginProfile('getViewCount');
		self::modelInfo($model, $className, $pk, $cacheId);
		$count = Yii::app()->cache->get($cacheId);
		if ($count === false) {
			$count = WikiView::model()->countByAttributes(array(
				'model'=>$className,
				'model_id'=>$pk
			));
			Yii::app()->cache->set($cacheId, $count);
		}
		Yii::endProfile('getViewCount');
		return $count;
	}
	
	public static function getTotalServed($userId)
	{
		$cacheId = self::getServedCacheId($userId);
		$served = Yii::app()->cache->get($cacheId);
		if ($served === false) {
			$served = WikiView::model()->countByAttributes(array('model_author'=>$userId));
			Yii::app()->cache->set($cacheId, $served);
		}
		return $served;
	}
	
	public static function getServedCacheId($authorId)
	{
		return "WikiView-served-$authorId";
	}
	
	public static function modelInfo($model, &$className, &$pk, &$cacheId='')
	{
		$className = get_class($model);
		$pk = $model->getPrimaryKey();
		$cacheId = "WikiView-$className-$pk-view-count";
	}
	
	/**
	 * list of view logs grouped by the user who viewed the model
	 * and ordered by date. So the viewed_on date is the most recent time
	 * they viewed
	 * @param CActiveRecord $model
	 * @return array WikiView models
	 */
	public static function getRecentViewers($model, $limit=10)
	{
		self::modelInfo($model, $className, $pk);
		$sql = "SELECT * FROM (SELECT viewed_by, viewed_on FROM wiki_view 
			WHERE model_id = $pk AND model = '$className'
			ORDER BY viewed_on DESC) AS tmp_table GROUP BY viewed_by limit $limit";
		// reorder by date
		$array = Yii::app()->db->createCommand($sql)->queryAll();
		// sort the array by latest first
		usort($array, function($a, $b){
			return -strcmp($a['viewed_on'], $b['viewed_on']);
		});
		return $array;
	}
	
	/**
	 * Gets users serverd by the $authorId
	 * @param int $authorId the User record id
	 * @return array WikiView records
	 */
	public static function getUsersServedBy($authorId)
	{
		return WikiView::model()->with('user')->findAllByAttributes(array('model_author'=>$authorId), array('order'=>'viewed_on DESC'));
	}
}
