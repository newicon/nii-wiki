<?php

/**
 * WikiQuestion class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Table of WikiQuestion records
 */
class WikiAnswer extends WikiQuestion
{
	
	public function tableName()
	{
		return '{{wiki_question}}';
	}
	
	/**
	 * get static model instance
	 * @param string $className
	 * @return WikiQuestion
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * core CActiveRecord Rules function
	 * @see CActiveRecord::rules
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('text', 'required'),
			array('title, text, question_id', 'safe')
		);
	}
	
	/**
	 * before saving we want to record the user id as this is the "owner" of the question
	 * @return type
	 */
	public function beforeSave()
	{
		$this->type = self::TYPE_ANSWER;
		return parent::beforeSave();
	}
	
}