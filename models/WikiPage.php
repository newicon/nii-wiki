<?php

/**
 * WikiPages class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Model representing wiki pages
 */
class WikiPage extends NActiveRecord
{
	/**
	 * Yii Standard function
	 * @see CActiveRecord::model
	 * @param string $className
	 * @return WikiPage static instance
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return '{{wiki_page}}';
	}
	
	public function rules()
	{
		return array(
			array('title', 'required'),
			array('title, slug, text, summary, published, featured', 'safe')
		);
	}
	
	/**
	 * Yii standard function
	 * define active record behaviors
	 * @see CActiveRecord::behaviors
	 * @return array
	 */
	public function behaviors()
	{
		return array(
			'timestampable'=>array(
				'class'=>'nii.components.behaviors.NTimestampable'
			),
			'versionable'=>array(
				'class'=>'nii.components.behaviors.NVersionable',
				'historyModelClass'=>'WikiPageHistory',
				'versionableModelClass'=>'WikiPage',
			),
			'tag'=>array(
               'class'=>'nii.components.behaviors.NTaggable'
            ),
			'oldattributes'=>array(
				'class'=>'nii.components.behaviors.NOldAttributesBehavior'
			),
			'trash'=>array(
				'class'=>'nii.components.behaviors.NTrashBinBehavior',
			),
		);
	}
	
	/**
	 * Scopes 
	 */
	public function scopes()
	{
		return array(
			'my'=>array(
				'condition'=>'author_id='.Yii::app()->user->id
			),
			'published'=>array(
                'condition'=>'published=1',
            ),
			'drafts'=>array(
				'condition'=>'published=0',
			),
			'featured'=>array(
				'condition'=>'featured=1'
			),
			'newest'=>array(
				'order'=>$this->getTableAlias().'.created_at desc'
			),
		);
	}
	
	/**
	 * gets articles based on when they were published
	 * @param int $limit
	 * @return WikiPage
	 */
	public function publishedRecently($limit=8)
	{
		$this->published()->getDbCriteria()->mergeWith(array(
			'order'=>'published_on DESC',
			'limit'=>$limit,
		));
		return $this;
	}
	
	/**
	 * get posts that have recently been updated
	 * good for getting a list of articles that are active
	 * @param int $limit
	 * @return WikiPage
	 */
	public function recently($limit=8)
	{
		$this->getDbCriteria()->mergeWith(array(
			'order'=>'updated_at DESC',
			'limit'=>$limit,
		));
		return $this;
	}
	
	/**
	 * limit results by author id, 
	 * if an authorId is not specified authorId will default to the current loggedin user id
	 * @param int $authorId
	 * @return WikiPage
	 */
	public function author($authorId=null)
	{
		if ($authorId===null)
			$authorId = Yii::app()->user->id;
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'author_id = '.$authorId,
		));
		return $this;
	}
	
	public function summary()
	{
		if (!empty($this->summary))
			return $this->summary;
		return NHtml::previewText($this->text, $charLimit);
	}
	
	/**
	 * hook into the before save function,
	 * sanitise the title to be url accessible
	 * @see CActiveRecord::beforeSave
	 * @return boolean
	 */
	public function beforeSave()
	{
		if (!isset($this->name_slug))
			$this->slug = NHtml::urlize($this->title);
		if ($this->isNewRecord)
			$this->author_id = Yii::app()->user->id;
		if ($this->attributeChanged('published') && $this->published == 1 
			|| $this->published == 1 && $this->isNewRecord) {
			$this->published_on = NTime::unixToDatetime();
		}
		return parent::beforeSave();
	}
	
	public function afterSave()
	{
		// the record has been set to published
		if ($this->attributeChanged('published') && $this->published == 1 
			|| $this->published == 1 && $this->isNewRecord) {
			NActivityLog::add("wiki", "article-published", array('link'=>$this->link, 'title'=>$this->title), $this, $this->id);
		}
		parent::afterSave();
		$this->luceneIndexUpdate();		
	}
	
	/**
	 * Nii standard function 
	 * schmea for this record
	 * @see NActiveRecord::schema
	 * @return array db schema
	 */
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				'title'=>'string',
				'slug'=>'string',
				// the markup - wiki markup of the page
				'summary'=>'string',
				'text'=>'longtext',
				'author_id'=>'int',
				'published'=>'int not null default \'0\'',
				'published_on'=>'datetime',
				'featured'=>'int not null default \'0\''
			),
			'keys'=>array(
				array('author_id'),
				array('slug_key', 'slug', true)
			)
		);
	}
	
	public function relations()
	{
		return array(
			'author'=>array(self::BELONGS_TO, 'User', 'author_id'),
		);
	}
	
	/**
	 * get the html link url for this record
	 * @see self::route
	 */
	public function getLink()
	{
		return NHtml::url($this->route);
	}
	
	/**
	 * get the route to this record
	 * @return array
	 */
	public function getRoute()
	{
		return array('/wiki/index/page', 'slug'=>$this->slug);
	}
	
	/**
	 * return the number of views this article has had
	 * @return int
	 */
	public function getViews()
	{
		return WikiView::getViewCount($this);
	}
	
	/**
	 * Get the number of history records
	 * @return int
	 */
	public function getRevisionsCount()
	{
		return WikiPageHistory::model()->countByAttributes(array('model_id'=>$this->id));
	}
	
	// lucene stuff
	///////////////////////
	
	/**
	 * TODO: move into search component / behavior
	 * @return type
	 */
	public function luceneLookupField()
	{
		return get_class($this) . '_id';
	}
	
	/**
	 * TODO: move into search component / behavior
	 * @return type
	 */
	public function luceneIndexUpdate()
	{
		Yii::import('wiki.vendor.*');
		require_once('Zend/Search/Lucene.php');
		$index = WikiSearch::get()->openIndex();
		 // find the document based on the indexed document_id field
		$term = new Zend_Search_Lucene_Index_Term($this->id, $this->luceneLookupField());
		foreach ($index->termDocs($term) as $id)
			$index->delete($id);

		// re-add the document
		$index->addDocument($this->luceneGetDocument());

		// write the index to disk
		$index->commit();
	}
	
	
	public function luceneGetDocument()
	{
		$doc = new Zend_Search_Lucene_Document();
		// add keyword so we can easily find this document when it is modified
		$doc->addField(Zend_Search_Lucene_Field::Keyword($this->luceneLookupField(), $this->id));
		$doc->addField(Zend_Search_Lucene_Field::Keyword('model', 'WikiPage'));
		$doc->addField(Zend_Search_Lucene_Field::unIndexed('pk', CHtml::encode($this->id), 'utf-8'));
		
		$doc->addField(Zend_Search_Lucene_Field::Text('title', CHtml::encode($this->title), 'utf-8'));
		$doc->addField(Zend_Search_Lucene_Field::Text('link', CHtml::encode($this->link), 'utf-8'));
		if ($this->author !== null) {
			$doc->addField(Zend_Search_Lucene_Field::Text('author', $this->author->name));
		}
		// index tags
		$tags = $this->tags;
		$doc->addField(Zend_Search_Lucene_Field::Text('tag', CHtml::encode(implode(',',$tags)), 'utf-8'));
		
		$doc->addField(Zend_Search_Lucene_Field::Text('link', CHtml::encode($this->link), 'utf-8'));
			
		if (!$this->summary) {
			$doc->addField(Zend_Search_Lucene_Field::Text('summary', substr($this->text, 0, 155), 'utf-8'));
		} else {
			$doc->addField(Zend_Search_Lucene_Field::Text('summary', $this->summary, 'utf-8'));
		}
		// index the content but don't bother storing it
		$doc->addField(Zend_Search_Lucene_Field::unStored('text', CHtml::encode($this->text), 'utf-8'));
		return $doc;
	}
	
}