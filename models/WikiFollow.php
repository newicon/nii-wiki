<?php

/**
 * WikiFollow class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of WikiFollow
 * Model to represent users following users.
 * Like twitter you can follow updates of specific users.
 */
class WikiFollow extends NActiveRecord
{
	public function tableName() 
	{
		return '{{wiki_follow}}';
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function schema()
	{
		return array(
			'columns'=>array(
				'user_id'=>'int',
				'follow_id'=>'int',
				'Primary key (user_id, follow_id)'
			)
		);
	}
	
	/**
	 * follow a user
	 * @param int $followId 
	 * @param int $userId | null then uses session user
	 */
	public static function follow($followId, $userId=null)
	{
		if ($userId===null)
			$userId = Yii::app()->user->record->id;
		return WikiFollow::model()->insert(array('user_id'=>$userId, 'follow_id'=>$followId));
	}
	
	/**
	 * Un follow a user
	 * @param int $followId
	 * @param int $userId | null then uses session user
	 * @return type
	 */
	public static function unFollow($followId, $userId=null)
	{
		if ($userId===null)
			$userId = Yii::app()->user->record->id;
		return WikiFollow::model()->deleteByPk(array('user_id'=>$userId, 'follow_id'=>$followId));
	}
	
	/**
	 * Get users I am following (I = $userId | null - current user)
	 * The users following the user defined by $userId are returned
	 * If no $userId provided, gets the followers for the current logged in user
	 * @param int $userId
	 */
	public static function getFollowing($userId=null)
	{
		if ($userId===null)
			$userId = Yii::app()->user->record->id;
		return WikiFollow::model()->findAllByAttributes(array('user_id'=>$userId));
	}
	
	/**
	 * Get users following me (I = $userId | null - current user)
	 * The users following the user defined by $userId are returned
	 * If no $userId provided, gets the followers for the current logged in user
	 * @param int $userId
	 */
	public static function getFollowers($userId=null)
	{
		if ($userId===null)
			$userId = Yii::app()->user->record->id;
		return WikiFollow::model()->findAllByAttributes(array('follow_id'=>$userId));
	}
	
	/**
	 * isFollowing()
	 * @param int $userId the user id to check if they are following $followingUserId
	 * @param int $followingUserId
	 */
	public static function isFollowing($followingUserId, $userId=null)
	{
		if ($userId===null)
			$userId = Yii::app()->user->record->id;
		
		$wf = WikiFollow::model()->findByPk(array('user_id'=>$userId, 'follow_id'=>$followingUserId));
		return ($wf === null) ? false : true;
	}
	
}