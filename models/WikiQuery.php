<?php

/**
 * WikiSearch class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Table to record all searches performed on the wiki,
 * this enables users to see what searches are trending thus help people :-)
 * This is called WikiQuery to avoid clashing with the helper component WikiSearch
 */
class WikiQuery extends NActiveRecord
{
	public function tableName()
	{
		return '{{wiki_query}}';
	}
	
	/**
	 * insert a new search query record
	 * @param string $query the search query
	 */
	public static function add($query)
	{
		$ws = new WikiQuery();
		$ws->q = $query;
		$ws->save();
	}
	
	/**
	 * get a list of the querys and their frequency
	 * @return type
	 */
	public static function searchFrequency()
	{
		$tbl = WikiQuery::model()->tableName();
		$sql = "SELECT q, count(*) as freq FROM $tbl GROUP BY q ORDER BY freq DESC";
		return Yii::app()->db->createCommand($sql)->queryAll();
	}
	
	/**
	 * get static model instance
	 * @param string $className
	 * @return WikiSearch
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * core CActiveRecord Rules function
	 * @see CActiveRecord::rules
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('q, created_at', 'safe')
		);
	}
	
	/**
	 * define model relations namely the relation to use user table
	 * for the user who performed the search query
	 * @return array
	 */
	public function relations()
	{
		return array(
			'user'=>array(self::BELONGS_TO, 'User', 'user_id')
		);
	}
	
	/**
	 * before saving we want to record the user id as this is the "owner" of the question
	 * @return type
	 */
	public function beforeSave()
	{
		if ($this->isNewRecord)
			$this->user_id = Yii::app()->user->id;
		return parent::beforeSave();
	}
	
	/**
	 * Core NActiveRecord function
	 * @see NActiveRecord::schema
	 * @return array
	 */
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				// the search string the user entered
				'q'=>'string',
				// the user who performed the search
				'user_id'=>'int',
				// timestamp uses the same name as the default timestampable 
				// behavior for consistency
				'created_at'=>'TIMESTAMP DEFAULT CURRENT_TIMESTAMP'
			)
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return NActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() 
	{
		$criteria = $this->getDbCriteria();

		$criteria->compare('q', $this->q, true);
		$criteria->compare($this->tableAlias.'.user_id', $this->user_id);
		$criteria->compare($this->tableAlias.'.created_at', $this->created_at);
		
		
		// Do all joins in the same SQL query
		$criteria->together  =  true;
		// Join the 'customer' table
		$criteria->with = array('user');
		
		$sort = new CSort;
		$sort->defaultOrder = $this->tableAlias.".created_at desc";
		
		$sort->attributes = array(
			'*', // add all of the other columns as sortable   
		); 
		
		return new NActiveDataProvider('WikiQuery', array(
			'criteria' => $criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>10,
			),
		));
	}
}