<?php

/**
 * WikiPages class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Model representing wiki pages
 */
class WikiPageHistory extends NActiveRecord
{
	/**
	 * Yii Standard function
	 * @see CActiveRecord::model
	 * @param string $className
	 * @return WikiPage static instance
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return '{{wiki_page_history}}';
	}
	
	/**
	 * Yii standard function
	 * define active record behaviors
	 * @see CActiveRecord::behaviors
	 * @return array
	 */
	public function behaviors()
	{
		return array(
			'versionable'=>array(
				'class'=>'nii.components.behaviors.NVersionable',
				'historyModelClass'=>'WikiPageHistory',
				'versionableModelClass'=>'WikiPage',
			),
			'timestampable'=>array(
				'class'=>'nii.components.behaviors.NTimestampable'
			),
		);
	}
}